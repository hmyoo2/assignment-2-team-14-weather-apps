function geo_success(position) {
    currentLat = position.coords.latitude;
    currentLon = position.coords.longitude;
    initMap(currentLat, currentLon);

}

function geo_error(position) {
    aler("Sorry, no geolocation support");
}


navigator.geolocation.getCurrentPosition(geo_success, geo_error);

function initMap(currentLat, currentLon) {
    var myLatLng = {
        lat: currentLat,
        lng: currentLon
    };
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: myLatLng
    });

    var Marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'position'
    });
    var url = "https://api.darksky.net/forecast/324cea7f0b848ca90efd0153271c0f68/"+currentLat+","+currentLon+"?callback=routesResponse";


    var script = document.createElement('script');
    script.src = url;
    document.body.appendChild(script);
}