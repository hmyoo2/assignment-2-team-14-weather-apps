function geo_success(position) {
    currentLat = position.coords.latitude;
    currentLon = position.coords.longitude;
    //initMap(currentLat,currentLon);

}

function geo_error(position) {
    aler("Sorry, no geolocation support");
}

navigator.geolocation.getCurrentPosition(geo_success, geo_error);

var data = {
    callback: "routesResponse"
};

var maxiTemperature = []; //create an empty array to input data
var miniTemperature = []; //create an empty array to input data
var images = []; //create an empty array to input data
var average = []; //create an empty array to input data
var icons = []; //create an empty array to input data
function routesResponse(data) //learnt from prac 6.1
{
    currenttemperature = convertTemp(data.currently.temperature); // extract out thedata we need 
    currenthumidity = data.currently.humidity * 100; // extract out the data we need 
    currentWindSpeed = Math.round(data.currently.windSpeed * 3.6); // extract out thedata we need 
    currentSummary = data.currently.summary; // extract out thedata we need 

    for (i = 0; i <= 7; i++) {

        maxiTemperature[i] = convertTemp(data.daily.data[i].apparentTemperatureMax); //the specific week we need
        miniTemperature[i] = convertTemp(data.daily.data[i].apparentTemperatureMin); //the specific week we need
        images[i] = data.daily.data[i].icon;
    }

    function addicon(images) { //all the png image coding



        if (images === "clear-day") { //based on the requested webpage
            return '<img src="images/clear-day.png" alt="clear-day" height ="20" width="20">';
        } else if (images === "clear-night") {
            return '<img src="images/clear-night.png" alt="clear-night" height ="20" width="20">';
        } else if (images === "rain") {
            return '<img src="images/rain.png" alt="rain" height ="20" width="20">';
        } else if (images === "snow") {
            return '<img src="images/snow.png" alt="snow" height ="20" width="20">';
        } else if (images === "sleet") {
            return '<img src="images/sleet.png" alt="sleet" height ="20" width="20">';
        } else if (images === "wind") {
            return '<img src="images/wind.png" alt="wind" height ="20" width="20">';
        } else if (images === "fog") {
            return '<img src="images/fog.png" alt="fog" height ="20" width="20">';
        } else if (images === "cloudy") {
            return '<img src="images/cloudy.png" alt="cloudy" height ="20" width="20">';
        } else if (images === "partly-cloudy-day") {
            return '<img src="images/partly-cloudy-day.png" alt="partly-cloudy-day" height ="20" width="20">';
        } else if (images === "partly-cloudy-night") {
            return '<img src= "images/partly-cloudy-night.png" alt="partly-cloudy-night" height ="20" width="20">';
        }
    }

    document.getElementById("icons1").innerHTML = addicon(images[1]); //input icon into specific place in html when json data matched
    document.getElementById("icons2").innerHTML = addicon(images[2]); //input icon into specific place in html when json data matched
    document.getElementById("icons3").innerHTML = addicon(images[3]); //input icon into specific place in html when json data matched
    document.getElementById("icons4").innerHTML = addicon(images[4]); //input icon into specific place in html when json data matched
    document.getElementById("icons5").innerHTML = addicon(images[5]); //input icon into specific place in html when json data matched
    document.getElementById("icons6").innerHTML = addicon(images[6]); //input icon into specific place in html when json data matched
    document.getElementById("icons7").innerHTML = addicon(images[7]); //input icon into specific place in html when json data matched




    average = Math.round((maxiTemperature[1] + maxiTemperature[2] + maxiTemperature[3] + maxiTemperature[4] + maxiTemperature[5] + maxiTemperature[6] + maxiTemperature[7]) / 7); //due the decimals, we round it up to clean it.

    document.getElementById("temperature").innerHTML = currenttemperature;
    document.getElementById("humidity").innerHTML = currenthumidity;
    document.getElementById("windSpeed").innerHTML = currentWindSpeed;
    document.getElementById("summary").innerHTML = currentSummary;

    document.getElementById("maxiTemperature1").innerHTML = maxiTemperature[1]; //input the weather data into specific place in html
    document.getElementById("maxiTemperature2").innerHTML = maxiTemperature[2]; //input the weather data into specific place in html
    document.getElementById("maxiTemperature3").innerHTML = maxiTemperature[3]; //input the weather data into specific place in html
    document.getElementById("maxiTemperature4").innerHTML = maxiTemperature[4]; //input the weather data into specific place in html
    document.getElementById("maxiTemperature5").innerHTML = maxiTemperature[5]; //input the weather data into specific place in html
    document.getElementById("maxiTemperature6").innerHTML = maxiTemperature[6]; //input the weather data into specific place in html
    document.getElementById("maxiTemperature7").innerHTML = maxiTemperature[7]; //input the weather data into specific place in html

    document.getElementById("miniTemperature1").innerHTML = miniTemperature[1]; //input the weather data into specific place in html
    document.getElementById("miniTemperature2").innerHTML = miniTemperature[2]; //input the weather data into specific place in html
    document.getElementById("miniTemperature3").innerHTML = miniTemperature[3]; //input the weather data into specific place in html
    document.getElementById("miniTemperature4").innerHTML = miniTemperature[4]; //input the weather data into specific place in html
    document.getElementById("miniTemperature5").innerHTML = miniTemperature[5]; //input the weather data into specific place in html
    document.getElementById("miniTemperature6").innerHTML = miniTemperature[6]; //input the weather data into specific place in html
    document.getElementById("miniTemperature7").innerHTML = miniTemperature[7]; //input the weather data into specific place in html

    document.getElementById("aveMax").innerHTML = average;
}

var now = new Date;
var nameList = ["Sunday", "Monday", "Tuesday", "Wedesday", "Tursday", "Friday", "Saturday", "Sunday", "Monday", "Tuesday", "Wedesday", "Tursday", "Friday", "Saturday"]; //easier to call 
var nameOfDay = []; //create an empty array to insert value
var t = 0;
var day = now.getDay()
for (i = day; i <= day + 7; i++) {
    nameOfDay[t] = nameList[i];
    t = t + 1;


}

document.getElementById("name0").innerHTML = nameOfDay[0]; //input daily names into specific places
document.getElementById("name1").innerHTML = nameOfDay[1]; //input daily names into specific places
document.getElementById("name2").innerHTML = nameOfDay[2]; //input daily names into specific places
document.getElementById("name3").innerHTML = nameOfDay[3]; //input daily names into specific places
document.getElementById("name4").innerHTML = nameOfDay[4]; //input daily names into specific places
document.getElementById("name5").innerHTML = nameOfDay[5]; //input daily names into specific places
document.getElementById("name6").innerHTML = nameOfDay[6]; //input daily names into specific places
document.getElementById("name7").innerHTML = nameOfDay[7]; //input daily names into specific places


function convertTemp(degree) {
    return Math.round((degree - 32) * (5 / 9));
}