//Page for adding location.
// declear varirables    
var map;
var address;
var geocoder;
// draw map
function initialMap() {

    map = new google.maps.Map(document.getElementById("map_addlocation")); //the location user input
    map.setCenter(new google.maps.LatLng(-37.912323, 145.132694), 1);
    map.setUIToDefault();
    geocoder = new GClientGeocoder();

}
// display search location on map
function search(address) {
    if (geocoder) {
        geocoder.getLatLng(
            address,
            function(point) {
                if (!point) {
                    alert(address + " not found");
                } else {
                    map.setCenter(point, 10); //go to the point with viewing distance of 10，set centre to search location 
                    var marker = new GMarker(point); //generate marker
                    map.addOverlay(marker); //display marker
                    marker.openInfoWindowHtml(address); //specific marker detail
                }
            }
        );
    }
}

//Calling weather api
var url = "https://api.darksky.net/forecast/324cea7f0b848ca90efd0153271c0f68/" + address + "?callback=routesResponse";


var data = {
    callback: "routesResponse"
};

var maxiTemperature = []; //create an empty array to input data
var miniTemperature = []; //create an empty array to input data
var images = []; //create an empty array to input data
var average = []; //create an empty array to input data
var icons = []; //create an empty array to input data
function routesResponse(data) //learnt from prac 6.1
{
    currenttemperature = convertTemp(data.currently.temperature); // extract out thedata we need 
    currenthumidity = data.currently.humidity * 100; // extract out the data we need 
    currentWindSpeed = Math.round(data.currently.windSpeed * 3.6); // extract out thedata we need 
    currentSummary = data.currently.summary; // extract out thedata we need 

    for (i = 0; i <= 7; i++) {

        maxiTemperature[i] = convertTemp(data.daily.data[i].apparentTemperatureMax); //the specific week we need
        miniTemperature[i] = convertTemp(data.daily.data[i].apparentTemperatureMin); //the specific week we need
        images[i] = data.daily.data[i].icon;
    }

    function addicon(images) { //all the png image coding



        if (images === "clear-day") { //based on the requested webpage
            return '<img src="images/clear-day.png" alt="clear-day" height ="20" width="20">';
        } else if (images === "clear-night") {
            return '<img src="images/clear-night.png" alt="clear-night" height ="20" width="20">';
        } else if (images === "rain") {
            return '<img src="images/rain.png" alt="rain" height ="20" width="20">';
        } else if (images === "cloudy") {
            return '<img src="images/cloudy.png" alt="cloudy" height ="20" width="20">';
        } else if (images === "partly-cloudy-day") {
            return '<img src="images/partly-cloudy-day.png" alt="partly-cloudy-day" height ="20" width="20">';
        } else if (images === "partly-cloudy-night") {
            return '<img src= "images/partly-cloudy-night.png" alt="partly-cloudy-night" height ="20" width="20">';
        } else if (images === "wind") {
            return '<img src="images/wind.png" alt="wind" height ="20" width="20">';
        } else if (images === "snow") {
            return '<img src="images/snow.png" alt="snow" height ="20" width="20">';
        } else if (images === "sleet") {
            return '<img src="images/sleet.png" alt="sleet" height ="20" width="20">';
        } else if (images === "fog") {
            return '<img src="images/fog.png" alt="fog" height ="20" width="20">';
        }

    }

    document.getElementById("icons1").innerHTML = addicon(images[1]); //input icon into specific place in html when json data matched
    document.getElementById("icons2").innerHTML = addicon(images[2]); //input icon into specific place in html when json data matched
    document.getElementById("icons3").innerHTML = addicon(images[3]); //input icon into specific place in html when json data matched
    document.getElementById("icons4").innerHTML = addicon(images[4]); //input icon into specific place in html when json data matched
    document.getElementById("icons5").innerHTML = addicon(images[5]); //input icon into specific place in html when json data matched
    document.getElementById("icons6").innerHTML = addicon(images[6]); //input icon into specific place in html when json data matched
    document.getElementById("icons7").innerHTML = addicon(images[7]); //input icon into specific place in html when json data matched




    average = Math.round((maxiTemperature[1] + maxiTemperature[2] + maxiTemperature[3] + maxiTemperature[4] + maxiTemperature[5] + maxiTemperature[6] + maxiTemperature[7]) / 7); //due the decimals, we round it up to clean it.

    document.getElementById("temperature").innerHTML = currenttemperature;
    document.getElementById("humidity").innerHTML = currenthumidity;
    document.getElementById("windSpeed").innerHTML = currentWindSpeed;
    document.getElementById("summary").innerHTML = currentSummary;


}