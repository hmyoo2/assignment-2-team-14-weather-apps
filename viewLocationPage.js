// Code for the View Location page.
// This is sample code to demonstrate navigation.
// You need not use it for final app.
var locationIndex = localStorage.getItem(APP_PREFIX + "-selectedLocation");
if (locationIndex !== null) {
    var locationNames = ["Location A", "Location B"];
    // If a location name was specified, use it for header bar title.
    document.getElementById("headerBarTitle").textContent = locationNames[locationIndex];
}

//URL for calling weather api with time factor.
var PastWeatherUrl = "https://api.darksky.net/forecast/324cea7f0b848ca90efd0153271c0f68/"+currentLat+","+currentLon+"?callback=routesResponse";
var PastWeatherSrc = document.createElement('PastWeatherSrc');
PastWeatherSrc.src = PastWeatherUrl;
document.body.appendChild(PastWeatherSrc);

var Pastdata = {
    callback: "PastResponse"
};
var maxiTemperature = []; //create an empty array to input data
var miniTemperature = []; //create an empty array to input data
var images = []; //create an empty array to input data
var average = []; //create an empty array to input data
var icons = []; //create an empty array to input data
function routesResponse(data) //learnt from prac 6.1
{
    currenttemperature = convertTemp(data.currently.temperature); // extract out thedata we need 
    currenthumidity = data.currently.humidity * 100; // extract out the data we need 
    currentWindSpeed = Math.round(data.currently.windSpeed * 3.6); // extract out thedata we need 
    currentSummary = data.currently.summary; // extract out thedata we need 

    for (i = 0; i <= 7; i++) {

        maxiTemperature[i] = convertTemp(data.daily.data[i].apparentTemperatureMax); //the specific week we need
        miniTemperature[i] = convertTemp(data.daily.data[i].apparentTemperatureMin); //the specific week we need
        images[i] = data.daily.data[i].icon;
    }

    function addicon(images) { //all the png image coding



        if (images === "clear-day") { //based on the requested webpage
            return '<img src="images/clear-day.png" alt="clear-day" height ="20" width="20">';
        } else if (images === "clear-night") {
            return '<img src="images/clear-night.png" alt="clear-night" height ="20" width="20">';
        } else if (images === "rain") {
            return '<img src="images/rain.png" alt="rain" height ="20" width="20">';
        } else if (images === "cloudy") {
            return '<img src="images/cloudy.png" alt="cloudy" height ="20" width="20">';
        } else if (images === "partly-cloudy-day") {
            return '<img src="images/partly-cloudy-day.png" alt="partly-cloudy-day" height ="20" width="20">';
        } else if (images === "partly-cloudy-night") {
            return '<img src= "images/partly-cloudy-night.png" alt="partly-cloudy-night" height ="20" width="20">';
        } else if (images === "wind") {
            return '<img src="images/wind.png" alt="wind" height ="20" width="20">';
        } else if (images === "snow") {
            return '<img src="images/snow.png" alt="snow" height ="20" width="20">';
        } else if (images === "sleet") {
            return '<img src="images/sleet.png" alt="sleet" height ="20" width="20">';
        } else if (images === "fog") {
            return '<img src="images/fog.png" alt="fog" height ="20" width="20">';
        }

    }

    document.getElementById("icons1").innerHTML = addicon(images[1]); //input icon into specific place in html when json data matched



    average = Math.round((maxiTemperature[1] + maxiTemperature[2] + maxiTemperature[3] + maxiTemperature[4] + maxiTemperature[5] + maxiTemperature[6] + maxiTemperature[7]) / 7); //due the decimals, we round it up to clean it.

    document.getElementById("temperature").innerHTML = currenttemperature;
    document.getElementById("humidity").innerHTML = currenthumidity;
    document.getElementById("windSpeed").innerHTML = currentWindSpeed;
    document.getElementById("summary").innerHTML = currentSummary;

    document.getElementById("maxiTemperature1").innerHTML = maxiTemperature[1]; //input the weather data into specific place in html

    document.getElementById("miniTemperature1").innerHTML = miniTemperature[1]; //input the weather data into specific place in html

    document.getElementById("aveMax").innerHTML = average;
}
// 3
var now = new Date;
var nameList = ["Sunday", "Monday", "Tuesday", "Wedesday", "Thursday", "Friday", "Saturday", "Sunday", "Monday", "Tuesday", "Wedesday", "Thursday", "Friday", "Saturday"]; //easier to call 
var nameOfDay = []; //create an empty array to insert value
var t = 0;
var j;
var day = //from slider 
    for (j = day; j <= day + 7; j++) {
        nameOfDay[t] = nameList[j];
        t = t + 1;


    }

document.getElementById("name0").innerHTML = nameOfDay[0]; //input daily names into specific places


function convertTemp(degree) {
    return Math.round((degree - 32) * (5 / 9));
}



function deleteData() {

    localStorage.removeItem(APP_PREFIX);
    window.location.href = " index.html";

}